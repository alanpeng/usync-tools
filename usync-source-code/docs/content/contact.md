---
title: "Contact"
description: "Contact the usync project"
---

# Contact the usync project #

## Forum ##

Forum for questions and general discussion:

  * https://forum.usync.uit.com.cn

## GitHub repository ##

The project's repository is located at:

  * https://github.com/rclone/rclone

There you can file bug reports or contribute with pull requests.

## Twitter ##

You can also follow me on twitter for usync announcements:

  * [@njcw](https://twitter.com/njcw)

## Email ##

Or if all else fails or you want to ask something private or
confidential email [Nick Craig-Wood](mailto:nick@craig-wood.com).
Please don't email me requests for help - those are better directed to
the forum. Thanks!
