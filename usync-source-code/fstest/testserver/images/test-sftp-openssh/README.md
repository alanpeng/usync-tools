# Test SFTP Openssh

This is a docker image for usync's integration tests which runs an
openssh server in a docker image.

## Build

```
docker build --rm -t usync/test-sftp-openssh .
docker push usync/test-sftp-openssh
```

# Test

```
usync lsf -R --sftp-host 172.17.0.2 --sftp-user usync --sftp-pass $(usync obscure password) :sftp:
```
