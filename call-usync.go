package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Println("please provide a command to run")
		os.Exit(1)
	}

	cmdStr := os.Args[1]
	args := strings.Split(cmdStr, " ")
	cmd := args[0]

	pid,err := exec.Command("pidof", cmd).Output()
	if err == nil && len(pid) > 0 {
		fmt.Printf("The command %s is already running\n", cmd)
		os.Exit(0)
	}

	c := exec.Command(cmd, args[1:]...)
	c.Stdin = os.Stdin
	c.Stdout = os.Stdout
	c.Stderr= os.Stderr

	err= c.Start()
	if err != nil {
		fmt.Printf("The command %s failed with start error: %s\n", cmdStr, err)
		os.Exit(1)
	}

	err = c.Wait()
	if err != nil {
		fmt.Printf("The command %s failed with wait error: %s\n", cmdStr, err)
		os.Exit(1)
	}
}
