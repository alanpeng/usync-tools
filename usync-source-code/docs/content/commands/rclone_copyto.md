---
title: "usync copyto"
description: "Copy files from source to dest, skipping identical files."
slug: usync_copyto
url: /commands/usync_copyto/
# autogenerated - DO NOT EDIT, instead edit the source code in cmd/copyto/ and as part of making a release run "make commanddocs"
---
# usync copyto

Copy files from source to dest, skipping identical files.

## Synopsis


If source:path is a file or directory then it copies it to a file or
directory named dest:path.

This can be used to upload single files to other than their current
name.  If the source is a directory then it acts exactly like the
[copy](/commands/usync_copy/) command.

So

    usync copyto src dst

where src and dst are usync paths, either remote:path or
/path/to/local or C:\windows\path\if\on\windows.

This will:

    if src is file
        copy it to dst, overwriting an existing file if it exists
    if src is directory
        copy it to dst, overwriting existing files if they exist
        see copy command for full details

This doesn't transfer files that are identical on src and dst, testing
by size and modification time or MD5SUM.  It doesn't delete files from
the destination.

**Note**: Use the `-P`/`--progress` flag to view real-time transfer statistics


```
usync copyto source:path dest:path [flags]
```

## Options

```
  -h, --help   help for copyto
```

See the [global flags page](/flags/) for global options not listed here.

## SEE ALSO

* [usync](/commands/usync/)	 - Show help for usync commands, flags and backends.

