#!/bin/bash
set -e
docker build -t usync/xgo-cgofuse https://github.com/winfsp/cgofuse.git
docker images
docker push usync/xgo-cgofuse
