package main

import (
    "fmt"
    "os"
    "os/exec"
    "strconv"
    "strings"
)

func main() {
    if len(os.Args) != 3 {
        fmt.Println("Usage: program process_name max_run_time_in_seconds")
        os.Exit(1)
    }

    processName := os.Args[1]
    maxRunTime, err := strconv.Atoi(os.Args[2])
    if err != nil {
        fmt.Println("Error parsing max run time:", err)
        os.Exit(1)
    }

    pid, err := exec.Command("pidof", processName).Output()
    if err != nil {
        // fmt.Println("Error getting process ID:", err)
        // os.Exit(1)
        fmt.Println("The process ", processName, "is not running!")
        os.Exit(0)
    }
    pidStr := strings.TrimSpace(string(pid))

    runTime, err := exec.Command("ps", "-p", pidStr, "-o", "etimes=").Output()
    if err != nil {
        fmt.Println("Error getting process run time:", err)
        os.Exit(1)
    }
    runTimeStr := strings.TrimSpace(string(runTime))
    runTimeInt, err := strconv.Atoi(runTimeStr)
    if err != nil {
        fmt.Println("Error parsing process run time:", err)
        os.Exit(1)
    }

    if runTimeInt > maxRunTime {
        err = exec.Command("kill", pidStr).Run()
        if err != nil {
            fmt.Println("Error killing process:", err)
            os.Exit(1)
        }
        fmt.Printf("Process %s has been running for %d seconds and exceeded the max run time of %d seconds. The process has been killed.\n", processName, runTimeInt, maxRunTime)
    } else {
        fmt.Printf("Process %s has been running for %d seconds which is within the max run time of %d seconds.\n", processName, runTimeInt, maxRunTime)
    }
}

