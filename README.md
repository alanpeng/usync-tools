# usync-tools

usync:		存储同步主程序

call-usync:	调用存储同步程序的外壳，保证usync不重复执行

watch-usync:	监测上述两个命令是否发生假死的工具，如果超时则杀死监测对象进程，防止同步因故障彻底失效

