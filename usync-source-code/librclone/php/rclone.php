<?php
/*
PHP interface to libusync.so, using FFI ( Foreign Function Interface )

Create an usync object

$rc = new Usync( __DIR__ . '/libusync.so' );

Then call rpc calls on it

    $rc->rpc( "config/listremotes", "{}" );

When finished, close it

    $rc->close();
*/

class Usync {

    protected $usync;
    private $out;

    public function __construct( $libshared )
    {
        $this->usync = \FFI::cdef("
        struct UsyncRPCResult {
            char* Output;
            int	Status;
        };        
        extern void UsyncInitialize();
        extern void UsyncFinalize();
        extern struct UsyncRPCResult UsyncRPC(char* method, char* input);
        extern void UsyncFreeString(char* str);
        ", $libshared);
        $this->usync->UsyncInitialize();
    }

    public function rpc( $method, $input ): array
    {
        $this->out = $this->usync->UsyncRPC( $method, $input );
        $response = [
            'output' => \FFI::string( $this->out->Output ),
            'status' => $this->out->Status
        ];
        $this->usync->UsyncFreeString( $this->out->Output );
        return $response;
    }

    public function close( ): void
    {
        $this->usync->UsyncFinalize();
    }
}
