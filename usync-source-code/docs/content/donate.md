---
title: "Donations"
description: "Donations to the usync project."
type: page
---

# {{< icon "fa fa-heart heart" >}} Donations to the usync project

Usync is a free open-source project with thousands of contributions
from volunteers all round the world and I would like to thank all of
you for donating your time to the project.

However, maintaining usync is a lot of work - easily the equivalent
of a **full time job** - for me. Nothing stands still in the world of
cloud storage.  Usync needs constant attention adapting to changes by
cloud providers, adding new providers, adding new features, keeping
the integration tests working, fixing bugs and many more things!

I love doing the work and I'd like to spend more time doing it - your
support helps make that possible.

Thank you :-)

{{< nick >}}

PS I'm available for usync and object storage related consultancy -
[email me](mailto:nick@craig-wood.com) for more info.

{{< monthly_donations >}}

## Personal users

If you are a personal user and you would like to support the project
with sponsorship as a way of saying thank you that would be most
appreciated. {{< icon "fa fa-heart heart" >}}

## Business users

If your business distributes usync as part of its products (which the
generous MIT licence allows) or uses it internally then it would make
business sense to sponsor the usync project to ensure that the
project you rely on stays healthy and well maintained.

If you run one of the cloud storage providers that usync supports and
usync is driving revenue your way then you know it makes sense to
sponsor the project. {{< icon "far fa-smile" >}}

Note that if you choose the "GitHub Sponsors" option they will provide
proper tax invoices appropriate for your country.

## Monthly donations

Monthly donations help keep usync development sustainable in the long
run so this is the preferred option. A small amount every month is
much better than a one off donation as it allows planning for the
future.

{{< monthly_donations >}}

## One off donations

If you don't want to contribute monthly then of course we'd love a one
off donation.

{{< one_off_donations >}}

If you require a receipt or wish to contribute in a different way then
please [drop me an email](mailto:nick@craig-wood.com).
