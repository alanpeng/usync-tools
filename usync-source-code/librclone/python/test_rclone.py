#!/usr/bin/env python3
"""
Test program for libusync
"""

import os
import subprocess
import unittest
from usync import *

class TestUsync(unittest.TestCase):
    """TestSuite for usync python module"""
    shared_object = "libusync.so"

    @classmethod
    def setUpClass(cls):
        super(TestUsync, cls).setUpClass()
        cls.shared_object = "./libusync.so"
        Usync.build(cls.shared_object)
        cls.usync = Usync(shared_object=cls.shared_object)

    @classmethod
    def tearDownClass(cls):
        cls.usync.close()
        os.remove(cls.shared_object)
        super(TestUsync, cls).tearDownClass()

    def test_rpc(self):
        o = self.usync.rpc("rc/noop", a=42, b="string", c=[1234])
        self.assertEqual(dict(a=42, b="string", c=[1234]), o)

    def test_rpc_error(self):
        try:
            o = self.usync.rpc("rc/error", a=42, b="string", c=[1234])
        except UsyncException as e:
            self.assertEqual(e.status, 500)
            self.assertTrue(e.output["error"].startswith("arbitrary error"))
        else:
            raise ValueError("Expecting exception")

if __name__ == '__main__':
    unittest.main()
