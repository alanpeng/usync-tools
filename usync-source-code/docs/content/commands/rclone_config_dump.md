---
title: "usync config dump"
description: "Dump the config file as JSON."
slug: usync_config_dump
url: /commands/usync_config_dump/
# autogenerated - DO NOT EDIT, instead edit the source code in cmd/config/dump/ and as part of making a release run "make commanddocs"
---
# usync config dump

Dump the config file as JSON.

```
usync config dump [flags]
```

## Options

```
  -h, --help   help for dump
```

See the [global flags page](/flags/) for global options not listed here.

## SEE ALSO

* [usync config](/commands/usync_config/)	 - Enter an interactive configuration session.

