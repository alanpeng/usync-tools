#!/usr/bin/env bash
exec usync --check-normalization=true --check-control=true --check-length=true info \
	/tmp/testInfo \
	TestAmazonCloudDrive:testInfo \
	TestB2:testInfo \
	TestCryptDrive:testInfo \
	TestCryptSwift:testInfo \
	TestDrive:testInfo \
	TestDropbox:testInfo \
	TestGoogleCloudStorage:usync-testinfo \
	TestnStorage:testInfo \
	TestOneDrive:testInfo \
	TestS3:usync-testinfo \
	TestSftp:testInfo \
	TestSwift:testInfo \
	TestYandex:testInfo \
	TestFTP:testInfo
