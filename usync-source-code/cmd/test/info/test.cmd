set RCLONE_CONFIG_LOCALWINDOWS_TYPE=local
usync.exe purge    LocalWindows:info
usync.exe info -vv LocalWindows:info --write-json=info-LocalWindows.json > info-LocalWindows.log  2>&1
usync.exe ls   -vv LocalWindows:info > info-LocalWindows.list 2>&1
