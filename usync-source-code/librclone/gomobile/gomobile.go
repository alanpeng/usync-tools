// Package gomobile exports shims for gomobile use
package gomobile

import (
	"github.com/rclone/rclone/libusync/libusync"

	_ "github.com/rclone/rclone/backend/all" // import all backends
	_ "github.com/rclone/rclone/lib/plugin"  // import plugins

	_ "golang.org/x/mobile/event/key" // make go.mod add this as a dependency
)

// UsyncInitialize initializes usync as a library
func UsyncInitialize() {
	libusync.Initialize()
}

// UsyncFinalize finalizes the library
func UsyncFinalize() {
	libusync.Finalize()
}

// UsyncRPCResult is returned from UsyncRPC
//
//	Output will be returned as a serialized JSON object
//	Status is a HTTP status return (200=OK anything else fail)
type UsyncRPCResult struct {
	Output string
	Status int
}

// UsyncRPC has an interface optimised for gomobile, in particular
// the function signature is valid under gobind rules.
//
// https://pkg.go.dev/golang.org/x/mobile/cmd/gobind#hdr-Type_restrictions
func UsyncRPC(method string, input string) (result *UsyncRPCResult) { //nolint:deadcode
	output, status := libusync.RPC(method, input)
	return &UsyncRPCResult{
		Output: output,
		Status: status,
	}
}
