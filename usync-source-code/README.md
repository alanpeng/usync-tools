[<img src="https://usync.uit.com.cn/img/logo_on_light__horizontal_color.svg" width="50%" alt="usync logo">](https://usync.uit.com.cn/#gh-light-mode-only)
[<img src="https://usync.uit.com.cn/img/logo_on_dark__horizontal_color.svg" width="50%" alt="usync logo">](https://usync.uit.com.cn/#gh-dark-mode-only)

[Website](https://usync.uit.com.cn) |
[Documentation](https://usync.uit.com.cn/docs/) |
[Download](https://usync.uit.com.cn/downloads/) |
[Contributing](CONTRIBUTING.md) |
[Changelog](https://usync.uit.com.cn/changelog/) |
[Installation](https://usync.uit.com.cn/install/) |
[Forum](https://forum.usync.uit.com.cn/)

[![Build Status](https://github.com/rclone/rclone/workflows/build/badge.svg)](https://github.com/rclone/rclone/actions?query=workflow%3Abuild)
[![Go Report Card](https://goreportcard.com/badge/github.com/rclone/rclone)](https://goreportcard.com/report/github.com/rclone/rclone)
[![GoDoc](https://godoc.org/github.com/rclone/rclone?status.svg)](https://godoc.org/github.com/rclone/rclone)
[![Docker Pulls](https://img.shields.io/docker/pulls/usync/usync)](https://hub.docker.com/r/usync/usync)

# Usync

Usync *("rsync for cloud storage")* is a command-line program to sync files and directories to and from different cloud storage providers.

## Storage providers

  * 1Fichier [:page_facing_up:](https://usync.uit.com.cn/fichier/)
  * Akamai Netstorage [:page_facing_up:](https://usync.uit.com.cn/netstorage/)
  * Alibaba Cloud (Aliyun) Object Storage System (OSS) [:page_facing_up:](https://usync.uit.com.cn/s3/#alibaba-oss)
  * Amazon Drive [:page_facing_up:](https://usync.uit.com.cn/amazonclouddrive/) ([See note](https://usync.uit.com.cn/amazonclouddrive/#status))
  * Amazon S3 [:page_facing_up:](https://usync.uit.com.cn/s3/)
  * Backblaze B2 [:page_facing_up:](https://usync.uit.com.cn/b2/)
  * Box [:page_facing_up:](https://usync.uit.com.cn/box/)
  * Ceph [:page_facing_up:](https://usync.uit.com.cn/s3/#ceph)
  * China Mobile Ecloud Elastic Object Storage (EOS) [:page_facing_up:](https://usync.uit.com.cn/s3/#china-mobile-ecloud-eos)
  * Cloudflare R2 [:page_facing_up:](https://usync.uit.com.cn/s3/#cloudflare-r2)
  * Arvan Cloud Object Storage (AOS) [:page_facing_up:](https://usync.uit.com.cn/s3/#arvan-cloud-object-storage-aos)
  * Citrix ShareFile [:page_facing_up:](https://usync.uit.com.cn/sharefile/)
  * DigitalOcean Spaces [:page_facing_up:](https://usync.uit.com.cn/s3/#digitalocean-spaces)
  * Digi Storage [:page_facing_up:](https://usync.uit.com.cn/koofr/#digi-storage)
  * Dreamhost [:page_facing_up:](https://usync.uit.com.cn/s3/#dreamhost)
  * Dropbox [:page_facing_up:](https://usync.uit.com.cn/dropbox/)
  * Enterprise File Fabric [:page_facing_up:](https://usync.uit.com.cn/filefabric/)
  * FTP [:page_facing_up:](https://usync.uit.com.cn/ftp/)
  * Google Cloud Storage [:page_facing_up:](https://usync.uit.com.cn/googlecloudstorage/)
  * Google Drive [:page_facing_up:](https://usync.uit.com.cn/drive/)
  * Google Photos [:page_facing_up:](https://usync.uit.com.cn/googlephotos/)
  * HDFS (Hadoop Distributed Filesystem) [:page_facing_up:](https://usync.uit.com.cn/hdfs/)
  * HiDrive [:page_facing_up:](https://usync.uit.com.cn/hidrive/)
  * HTTP [:page_facing_up:](https://usync.uit.com.cn/http/)
  * Huawei Cloud Object Storage Service(OBS) [:page_facing_up:](https://usync.uit.com.cn/s3/#huawei-obs)
  * Internet Archive [:page_facing_up:](https://usync.uit.com.cn/internetarchive/)
  * Jottacloud [:page_facing_up:](https://usync.uit.com.cn/jottacloud/)
  * IBM COS S3 [:page_facing_up:](https://usync.uit.com.cn/s3/#ibm-cos-s3)
  * IONOS Cloud [:page_facing_up:](https://usync.uit.com.cn/s3/#ionos)
  * Koofr [:page_facing_up:](https://usync.uit.com.cn/koofr/)
  * Mail.ru Cloud [:page_facing_up:](https://usync.uit.com.cn/mailru/)
  * Memset Memstore [:page_facing_up:](https://usync.uit.com.cn/swift/)
  * Mega [:page_facing_up:](https://usync.uit.com.cn/mega/)
  * Memory [:page_facing_up:](https://usync.uit.com.cn/memory/)
  * Microsoft Azure Blob Storage [:page_facing_up:](https://usync.uit.com.cn/azureblob/)
  * Microsoft OneDrive [:page_facing_up:](https://usync.uit.com.cn/onedrive/)
  * Minio [:page_facing_up:](https://usync.uit.com.cn/s3/#minio)
  * Nextcloud [:page_facing_up:](https://usync.uit.com.cn/webdav/#nextcloud)
  * OVH [:page_facing_up:](https://usync.uit.com.cn/swift/)
  * OpenDrive [:page_facing_up:](https://usync.uit.com.cn/opendrive/)
  * OpenStack Swift [:page_facing_up:](https://usync.uit.com.cn/swift/)
  * Oracle Cloud Storage [:page_facing_up:](https://usync.uit.com.cn/swift/)
  * Oracle Object Storage [:page_facing_up:](https://usync.uit.com.cn/oracleobjectstorage/)
  * ownCloud [:page_facing_up:](https://usync.uit.com.cn/webdav/#owncloud)
  * pCloud [:page_facing_up:](https://usync.uit.com.cn/pcloud/)
  * premiumize.me [:page_facing_up:](https://usync.uit.com.cn/premiumizeme/)
  * put.io [:page_facing_up:](https://usync.uit.com.cn/putio/)
  * QingStor [:page_facing_up:](https://usync.uit.com.cn/qingstor/)
  * Qiniu Cloud Object Storage (Kodo) [:page_facing_up:](https://usync.uit.com.cn/s3/#qiniu)
  * Rackspace Cloud Files [:page_facing_up:](https://usync.uit.com.cn/swift/)
  * RackCorp Object Storage [:page_facing_up:](https://usync.uit.com.cn/s3/#RackCorp)
  * Scaleway [:page_facing_up:](https://usync.uit.com.cn/s3/#scaleway)
  * Seafile [:page_facing_up:](https://usync.uit.com.cn/seafile/)
  * SeaweedFS [:page_facing_up:](https://usync.uit.com.cn/s3/#seaweedfs)
  * SFTP [:page_facing_up:](https://usync.uit.com.cn/sftp/)
  * SMB / CIFS [:page_facing_up:](https://usync.uit.com.cn/smb/)
  * StackPath [:page_facing_up:](https://usync.uit.com.cn/s3/#stackpath)
  * Storj [:page_facing_up:](https://usync.uit.com.cn/storj/)
  * SugarSync [:page_facing_up:](https://usync.uit.com.cn/sugarsync/)
  * Tencent Cloud Object Storage (COS) [:page_facing_up:](https://usync.uit.com.cn/s3/#tencent-cos)
  * Wasabi [:page_facing_up:](https://usync.uit.com.cn/s3/#wasabi)
  * WebDAV [:page_facing_up:](https://usync.uit.com.cn/webdav/)
  * Yandex Disk [:page_facing_up:](https://usync.uit.com.cn/yandex/)
  * Zoho WorkDrive [:page_facing_up:](https://usync.uit.com.cn/zoho/)
  * The local filesystem [:page_facing_up:](https://usync.uit.com.cn/local/)

Please see [the full list of all storage providers and their features](https://usync.uit.com.cn/overview/)

### Virtual storage providers

These backends adapt or modify other storage providers

  * Alias: rename existing remotes [:page_facing_up:](https://usync.uit.com.cn/alias/)
  * Cache: cache remotes (DEPRECATED) [:page_facing_up:](https://usync.uit.com.cn/cache/)
  * Chunker: split large files [:page_facing_up:](https://usync.uit.com.cn/chunker/)
  * Combine: combine multiple remotes into a directory tree [:page_facing_up:](https://usync.uit.com.cn/combine/)
  * Compress: compress files [:page_facing_up:](https://usync.uit.com.cn/compress/)
  * Crypt: encrypt files [:page_facing_up:](https://usync.uit.com.cn/crypt/)
  * Hasher: hash files [:page_facing_up:](https://usync.uit.com.cn/hasher/)
  * Union: join multiple remotes to work together [:page_facing_up:](https://usync.uit.com.cn/union/)

## Features

  * MD5/SHA-1 hashes checked at all times for file integrity
  * Timestamps preserved on files
  * Partial syncs supported on a whole file basis
  * [Copy](https://usync.uit.com.cn/commands/usync_copy/) mode to just copy new/changed files
  * [Sync](https://usync.uit.com.cn/commands/usync_sync/) (one way) mode to make a directory identical
  * [Check](https://usync.uit.com.cn/commands/usync_check/) mode to check for file hash equality
  * Can sync to and from network, e.g. two different cloud accounts
  * Optional large file chunking ([Chunker](https://usync.uit.com.cn/chunker/))
  * Optional transparent compression ([Compress](https://usync.uit.com.cn/compress/))
  * Optional encryption ([Crypt](https://usync.uit.com.cn/crypt/))
  * Optional FUSE mount ([usync mount](https://usync.uit.com.cn/commands/usync_mount/))
  * Multi-threaded downloads to local disk
  * Can [serve](https://usync.uit.com.cn/commands/usync_serve/) local or remote files over HTTP/WebDAV/FTP/SFTP/DLNA

## Installation & documentation

Please see the [usync website](https://usync.uit.com.cn/) for:

  * [Installation](https://usync.uit.com.cn/install/)
  * [Documentation & configuration](https://usync.uit.com.cn/docs/)
  * [Changelog](https://usync.uit.com.cn/changelog/)
  * [FAQ](https://usync.uit.com.cn/faq/)
  * [Storage providers](https://usync.uit.com.cn/overview/)
  * [Forum](https://forum.usync.uit.com.cn/)
  * ...and more

## Downloads

  * https://usync.uit.com.cn/downloads/

License
-------

This is free software under the terms of the MIT license (check the
[COPYING file](/COPYING) included in this package).
