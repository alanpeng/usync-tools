"""
Python interface to libusync.so using ctypes

Create an usync object

    usync = Usync(shared_object="/path/to/libusync.so")

Then call rpc calls on it

    usync.rpc("rc/noop", a=42, b="string", c=[1234])

When finished, close it

    usync.close()
"""

__all__ = ('Usync', 'UsyncException')

import os
import json
import subprocess
from ctypes import *

class UsyncRPCString(c_char_p):
    """
    This is a raw string from the C API

    With a plain c_char_p type, ctypes will replace it with a
    regular Python string object that cannot be used with
    UsyncFreeString. Subclassing prevents it, while the string
    can still be retrieved from attribute value.
    """
    pass

class UsyncRPCResult(Structure):
    """
    This is returned from the C API when calling UsyncRPC
    """
    _fields_ = [("Output", UsyncRPCString),
                ("Status", c_int)]

class UsyncException(Exception):
    """
    Exception raised from usync

    This will have the attributes:

    output - a dictionary from the call
    status - a status number
    """
    def __init__(self, output, status):
        self.output = output
        self.status = status
        message = self.output.get('error', 'Unknown usync error')
        super().__init__(message)

class Usync():
    """
    Interface to Usync via libusync.so

    Initialise with shared_object as the file path of libusync.so
    """
    def __init__(self, shared_object=f"./libusync{'.dll' if os.name == 'nt' else '.so'}"):
        self.usync = CDLL(shared_object)
        self.usync.UsyncRPC.restype = UsyncRPCResult
        self.usync.UsyncRPC.argtypes = (c_char_p, c_char_p)
        self.usync.UsyncFreeString.restype = None
        self.usync.UsyncFreeString.argtypes = (c_char_p,)
        self.usync.UsyncInitialize.restype = None
        self.usync.UsyncInitialize.argtypes = ()
        self.usync.UsyncFinalize.restype = None
        self.usync.UsyncFinalize.argtypes = ()
        self.usync.UsyncInitialize()
    def rpc(self, method, **kwargs):
        """
        Call an usync RC API call with the kwargs given.

        The result will be a dictionary.

        If an exception is raised from usync it will of type
        UsyncException.
        """
        method = method.encode("utf-8")
        parameters = json.dumps(kwargs).encode("utf-8")
        resp = self.usync.UsyncRPC(method, parameters)
        output = json.loads(resp.Output.value.decode("utf-8"))
        self.usync.UsyncFreeString(resp.Output)
        status = resp.Status
        if status != 200:
            raise UsyncException(output, status)
        return output
    def close(self):
        """
        Call to finish with the usync connection
        """
        self.usync.UsyncFinalize()
        self.usync = None
    @classmethod
    def build(cls, shared_object):
        """
        Builds usync to shared_object if it doesn't already exist

        Requires go to be installed
        """
        if os.path.exists(shared_object):
            return
        print("Building "+shared_object)
        subprocess.check_call(["go", "build", "--buildmode=c-shared", "-o", shared_object, "github.com/rclone/rclone/libusync"])
