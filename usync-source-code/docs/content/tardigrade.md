---
title: "Tardigrade"
description: "Usync docs for Tardigrade"
---

# {{< icon "fas fa-dove" >}} Tardigrade

The Tardigrade backend has been renamed to be the [Storj backend](/storj/).
Old configuration files will continue to work.
